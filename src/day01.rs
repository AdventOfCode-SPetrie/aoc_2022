use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day01/problem.txt");
    println!("Advent of Code: 2022-12-01");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[u32]) -> u32 {
    let mut result: u32 = 0;
    for value in sum_elf_calories(input) {
        match value {
            v if v > result => result = v,
            _ => {}
        }
    }
    result
}

pub fn part_two(input: &[u32]) -> u32 {
    let mut values: [u32; 3] = [0, 0, 0];
    for value in sum_elf_calories(input) {
        match value {
            v if v > values[0] => {
                values[2] = values[1];
                values[1] = values[0];
                values[0] = v;
            }
            v if v > values[1] => {
                values[2] = values[1];
                values[1] = v;
            }
            v if v > values[2] => {
                values[2] = v;
            }
            _ => {}
        }
    }
    values.iter().sum()
}

pub fn sum_elf_calories(input: &[u32]) -> Vec<u32> {
    let mut values: Vec<u32> = Vec::new();
    let mut sum: u32 = 0;
    for number in input {
        match *number {
            0 => {
                values.push(sum);
                sum = 0;
            }
            n => sum += n,
        }
    }
    values
}

pub fn parse_input(input: &str) -> Vec<u32> {
    let mut values: Vec<u32> = Vec::with_capacity(2000);

    let bytes = input.as_bytes();
    let mut value: u32 = 0;
    for byte in bytes {
        match *byte {
            b'\n' => {
                values.push(value);
                value = 0;
            }
            v => {
                value *= 10;
                value += (v - b'0') as u32;
            }
        }
    }
    values.push(value);
    values
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day01/problem.txt",    66306),
        part_one_1:         ("inputs/day01/test01.txt",     24000),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day01/problem.txt",    195292),
        part_two_1:         ("inputs/day01/test01.txt",     45000),
    }
}
