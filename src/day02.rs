use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day02/problem.txt");
    println!("Advent of Code: 2022-12-02");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    input.iter().for_each(|v| println!("{}", v));
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

const AX: usize = 0;
const BX: usize = 1;
const CX: usize = 2;
const AY: usize = 3;
const BY: usize = 4;
const CY: usize = 5;
const AZ: usize = 6;
const BZ: usize = 7;
const CZ: usize = 8;

pub fn part_one(input: &[u32]) -> u32 {
    input[BX] + input[CY] * 2 + input[AZ] * 3 + input[AX] * 4 + input[BY] * 5 + input[CZ] * 6 + input[CX] * 7 + input[AY] * 8 + input[BZ] * 9
}

pub fn part_two(input: &[u32]) -> u32 {
    input[BX] + input[CX] * 2 + input[AX] * 3 + input[AY] * 4 + input[BY] * 5 + input[CY] * 6 + input[CZ] * 7 + input[AZ] * 8 + input[BZ] * 9
}

pub fn parse_input(input: &str) -> [u32; 9] {
    let mut values: [u32; 9] = [0;9];

    let bytes = input.as_bytes();
    let mut index: usize = 0;
    while index < bytes.len() {
        values[usize::from((bytes[index] - b'A') + ((bytes[index + 2] - b'X') * 3))] += 1;
        index += 4;
    }
    values
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day02/problem.txt",    12740),
        part_one_1:         ("inputs/day02/test01.txt",     15),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day02/problem.txt",    11980),
        part_two_1:         ("inputs/day02/test01.txt",     12),
    }
}
