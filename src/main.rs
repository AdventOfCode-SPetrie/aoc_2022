use std::time::Instant;

mod day01;
mod day02;

fn main() {
    let start = Instant::now();
    day01::exec();
    day02::exec();
    println!(
        "Advent of Code 2021 Total Time: {}ms\n",
        start.elapsed().as_millis()
    );
}
