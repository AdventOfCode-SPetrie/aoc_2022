use aoc_2022::*;
use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    //Day01
    let raw_input = file_input::read_to_string("inputs/day01/problem.txt");
    let input = day01::parse_input(&raw_input);
    c.bench_function("Day01:Parse", |b| {
        b.iter(|| {
            let _test = day01::parse_input(&raw_input);
        })
    });
    c.bench_function("Day01:P1", |b| {
        b.iter(|| day01::part_one(black_box(&input)))
    });
    c.bench_function("Day01:P2", |b| {
        b.iter(|| day01::part_two(black_box(&input)))
    });
    //Day02
    let raw_input = file_input::read_to_string("inputs/day02/problem.txt");
    let input = day02::parse_input(&raw_input);
    c.bench_function("Day02:Parse", |b| {
        b.iter(|| {
            let _test = day02::parse_input(&raw_input);
        })
    });
    c.bench_function("Day02:P1", |b| {
        b.iter(|| day02::part_one(black_box(&input)))
    });
    c.bench_function("Day02:P2", |b| {
        b.iter(|| day02::part_two(black_box(&input)))
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
