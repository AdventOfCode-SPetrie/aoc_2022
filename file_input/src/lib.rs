use std::fmt::Debug;
use std::fs;
use std::path::Path;
use std::str::FromStr;

pub fn input_from_file<T, E>(filename: impl AsRef<Path>) -> Vec<T>
where
    T: FromStr<Err = E>,
    E: Debug,
{
    fs::read_to_string(filename)
        .unwrap()
        .trim()
        .lines()
        .map(|v| v.parse().unwrap())
        .collect::<Vec<T>>()
}

pub fn read_to_string(filename: impl AsRef<Path>) -> String {
    fs::read_to_string(filename).unwrap().replace("\r\n", "\n")
}

pub fn read_as_bytes(filename: impl AsRef<Path>) -> Vec<u8> {
    fs::read(filename).unwrap()
}
